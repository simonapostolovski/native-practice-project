import {
  CLEAR_SIGNEDUP,
  ERR,
  LOADING,
  LOGIN,
  SIGNUP,
} from "../Constants/Contstants";
import firebase from "firebase";
import AsyncStorage from "@react-native-async-storage/async-storage";
export const LoginHandle = (user) => {
  console.log(user);
  return function (dispatch) {
    dispatch({ type: LOADING });
    firebase
      .auth()
      .signInWithEmailAndPassword(user.email, user.password)
      .then((res) => {
        dispatch({
          type: SAVE_USER,
          payload: res.user,
        });
      })
      .catch((error) => {
        console.log(error);
        dispatch({
          type: ERR,
          payload: "Incorrect Email or Password. Try again!",
        });
      });
  };
};

export const SignupHandle = (user) => {
  return function (dispatch) {
    dispatch({ type: LOADING });
    firebase
      .auth()
      .createUserWithEmailAndPassword(user.email, user.password)
      .then((res) => {
        res.user.updateProfile({
          displayName: user.displayName,
        });
        dispatch({
          type: SIGNUP,
        });
      })
      .catch((error) => {
        console.log(error);
        dispatch({
          type: ERR,
          payload: "Something went wrong, please try again!",
        });
      });
  };
};

export const handleLogout = () => {
  return function (dispatch) {
    firebase
      .auth()
      .signOut()
      .then((res) => {
        dispatch({ type: CLEAR_SIGNEDUP });
        AsyncStorage.clear();
      })
      .catch((err) => console.log(err));
  };
};
