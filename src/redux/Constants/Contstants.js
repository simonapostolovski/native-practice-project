export const LOGIN = "LOGIN";
export const SIGNUP = "SIGNUP";
export const ERR = "ERR";
export const LOADING = "LOADING";
export const CLEAR_SIGNEDUP = "CLEAR_SIGNEDUP";
export const LOGOUT = "LOGOUT";
export const LOGGED_IN = "LOGGED_IN";
export const SAVE_USER = "SAVE_USER";
