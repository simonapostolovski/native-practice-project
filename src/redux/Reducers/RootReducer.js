import AsyncStorage from "@react-native-async-storage/async-storage";
import {
  CLEAR_SIGNEDUP,
  ERR,
  LOADING,
  LOGGED_IN,
  LOGIN,
  SAVE_USER,
  SIGNUP,
} from "../Constants/Contstants";
const initialState = {
  isLoggedIn: false,
  isLoading: false,
  _user: null,
  err: null,
  signedUp: false,
};

export const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      // console.log("MOJOT USER >>", AsyncStorage.getItem("USER"));
      // console.log("userpayload>>", action.payload);
      // AsyncStorage.setItem('USER', (action.payload))
      return {
        ...state,
        _user: action.payload,
        // isLoading: false,
        isLoggedIn: true,
      };

    case SAVE_USER:
      console.log(action.payload);
      return {
        ...state,
        isLoggedIn: true,
        isLoading: false,
        _user: action.payload,
      };

    case LOGGED_IN:
      // console.log("MOJOT USER >>", AsyncStorage.getItem("USER"));

      return {
        ...state,
        isLoggedIn: true,
        isLoading: false,
        // _user: AsyncStorage.getItem("USER"),
      };

    case SIGNUP:
      return {
        ...state,
        isLoading: false,
        signedUp: true,
      };

    case CLEAR_SIGNEDUP:
      return {
        ...state,
        isLoggedIn: false,
        isLoading: false,
        _user: null,
        err: null,
        signedUp: false,
      };

    case ERR:
      return {
        ...state,
        err: action.payload,
        isLoading: false,
      };

    case LOADING:
      return {
        ...state,
        isLoading: true,
      };
    default:
      break;
  }
  return state;
};
