import React, { useEffect, useState } from "react";
import {
  ActivityIndicator,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import Ionicons from "react-native-vector-icons/Ionicons";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { useDispatch, useSelector } from "react-redux";
import { SignupHandle } from "../redux/Actions/Actions";
import { CLEAR_SIGNEDUP } from "../redux/Constants/Contstants";

const SigUpForm = ({ navigation }) => {
  const dispatch = useDispatch();
  const lock = <FontAwesome name="lock" size={32} color="green" />;
  const envelope = <EvilIcons name="envelope" size={32} color="green" />;
  const person = <Ionicons name="person" size={32} color="green" />;

  const { isLoggedIn, isLoading, err, signedUp } = useSelector((state) => ({
    isLoggedIn: state.isLoggedIn,
    isLoading: state.isLoading,
    err: state.err,
    signedUp: state.signedUp,
  }));

  const handleSignup = () => {
    if (!email.includes("@") || email.length < 5) {
      setEmailErr("Enter a valid Email!");
      if (password < 6) {
        setPasswordErr("Password must have atleast 6 chars.");
      }
    } else {
      setEmailErr(null);
      setPasswordErr(null);

      const userCredentials = {
        email,
        password,
        displayName,
      };
      dispatch(SignupHandle(userCredentials));
    }
  };

  const [emailErr, setEmailErr] = useState(null);
  const [passwordErr, setPasswordErr] = useState(null);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [displayName, setDisplayName] = useState("");

  return (
    <SafeAreaView>
      {signedUp ? (
        <TouchableOpacity
          onPress={() => {
            navigation.navigate("Login");
            dispatch({ type: CLEAR_SIGNEDUP });
          }}
        >
          <Text style={{ color: "darkblue", fontSize: 22 }}>
            Your account has been succesfully created.
          </Text>
          <Text style={{ color: "darkblue", fontSize: 22 }}>
            Click here to Log In.
          </Text>
        </TouchableOpacity>
      ) : (
        <>
          {isLoading ? (
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                height: "100%",
              }}
            >
              <Text style={{ fontSize: 28, color: "black", marginBottom: 20 }}>
                Standby..
              </Text>
              <ActivityIndicator size={72} color="blue" />
            </View>
          ) : (
            <>
              <Text
                style={{
                  fontSize: 32,
                  textAlign: "center",
                  padding: 10,
                  marginTop: 15,
                  color: err ? "red" : "black",
                }}
              >
                {err !== null ? err : "Sign Up"}
              </Text>
              <View style={styles.form}>
                <View style={styles.inputView}>
                  <View
                    style={{
                      flexDirection: "row",
                      height: 50,
                    }}
                  >
                    <Text style={styles.iconView}>{person}</Text>
                    <TextInput
                      style={styles.input}
                      placeholder="Display Name"
                      autoCorrect={false}
                      autoCapitalize="none"
                      value={displayName}
                      onChangeText={(e) => setDisplayName(e)}
                    />
                  </View>
                </View>
                <View style={styles.inputView}>
                  <View
                    style={{
                      flexDirection: "row",
                      height: 50,
                    }}
                  >
                    <Text style={styles.iconView}>{envelope}</Text>
                    <TextInput
                      style={styles.input}
                      placeholder="Email"
                      autoCorrect={false}
                      autoCapitalize="none"
                      value={email}
                      onChangeText={(e) => setEmail(e)}
                    />
                  </View>
                  {emailErr ? (
                    <View
                      style={{
                        height: 2,
                      }}
                    >
                      <Text style={{ color: "red" }}>{emailErr}</Text>
                    </View>
                  ) : null}
                </View>
                <View style={styles.inputView}>
                  <Text
                    style={{
                      position: "absolute",
                      right: 50,
                      top: 3,
                      fontSize: 12,
                      color: "black",
                    }}
                  >
                    Atleast 6 char. long and have one capital letter.
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      height: 50,
                    }}
                  >
                    <Text style={styles.iconView}>{lock}</Text>
                    <TextInput
                      style={styles.input}
                      placeholder="Password"
                      autoCorrect={false}
                      autoCapitalize="none"
                      value={password}
                      secureTextEntry
                      onChangeText={(e) => setPassword(e)}
                    />
                  </View>
                  {passwordErr ? (
                    <View
                      style={{
                        height: 2,
                      }}
                    >
                      <Text style={{ color: "red" }}>{passwordErr}</Text>
                    </View>
                  ) : null}
                </View>
                <TouchableOpacity
                  style={styles.signupBtn}
                  onPress={handleSignup}
                >
                  <Text
                    style={{
                      fontSize: 28,
                      padding: 10,
                      color: "black",
                      fontWeight: "bold",
                    }}
                  >
                    Sign Up
                  </Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                style={styles.toSignup}
                onPress={() => navigation.navigate("Login")}
              >
                <Text style={{ color: "darkblue", fontSize: 22 }}>
                  Already signed up?
                </Text>
                <Text style={{ color: "darkblue", fontSize: 22 }}>
                  Click here to log in!
                </Text>
              </TouchableOpacity>
            </>
          )}
        </>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  form: {
    marginTop: 25,
    paddingHorizontal: 20,
    justifyContent: "center",
    alignItems: "center",
  },

  inputView: {
    height: 120,
    width: "95%",
    flexDirection: "column",
    justifyContent: "space-evenly",
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 10,
    marginBottom: 25,
    alignItems: "center",
    position: "relative",
  },

  input: {
    width: "80%",
    borderBottomWidth: 1,
    borderBottomColor: "skyblue",
    fontSize: 18,
  },

  iconView: {
    width: "10%",
    alignSelf: "center",
  },

  signupBtn: {
    borderWidth: 1,
    borderColor: "black",
    marginTop: 10,
    borderRadius: 10,
    paddingHorizontal: 15,
    paddingVertical: 8,
  },

  toSignup: {
    marginTop: 50,
    marginLeft: 30,
  },
});

export default SigUpForm;
