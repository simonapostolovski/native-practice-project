import React, { useState } from "react";
import {
  ActivityIndicator,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { Input } from "react-native-elements";
import { TextInput } from "react-native-gesture-handler";
import Ionicons from "react-native-vector-icons/Ionicons";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { useDispatch, useSelector } from "react-redux";
import { LOGIN } from "../redux/Constants/Contstants";
import { LoginHandle } from "../redux/Actions/Actions";

const LoginForm = ({ navigation }) => {
  const lock = <FontAwesome name="lock" size={32} color="green" />;
  const envelope = <EvilIcons name="envelope" size={32} color="green" />;
  const dispatch = useDispatch();

  const { isLoggedIn, isLoading, err } = useSelector((state) => ({
    isLoggedIn: state.isLoggedIn,
    isLoading: state.isLoading,
    err: state.err,
  }));

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = () => {
    const userCredentials = {
      email,
      password,
    };
    dispatch(LoginHandle(userCredentials));
  };

  return (
    <SafeAreaView>
      {isLoading ? (
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            height: "100%",
          }}
        >
          <Text style={{ fontSize: 28, color: "black", marginBottom: 20 }}>
            Standby..
          </Text>
          <ActivityIndicator size={72} color="blue" />
        </View>
      ) : (
        <>
          <Text
            style={{
              fontSize: 32,
              textAlign: "center",
              padding: 10,
              marginTop: 15,
              color: err ? "red" : "black",
            }}
          >
            {err !== null ? err : "Log In"}
          </Text>
          <View style={styles.form}>
            <View style={styles.inputView}>
              <Text style={styles.iconView}>{envelope}</Text>
              <TextInput
                style={styles.input}
                placeholder="Email"
                autoCorrect={false}
                autoCapitalize="none"
                value={email}
                onChangeText={(e) => setEmail(e)}
              />
            </View>
            <View style={styles.inputView}>
              <Text style={styles.iconView}>{lock}</Text>
              <TextInput
                style={styles.input}
                placeholder="Password"
                secureTextEntry
                autoCorrect={false}
                autoCapitalize="none"
                value={password}
                onChangeText={(e) => setPassword(e)}
              />
            </View>
            <TouchableOpacity style={styles.loginBtn} onPress={handleLogin}>
              <Text
                style={{
                  fontSize: 28,
                  padding: 10,
                  color: "black",
                  fontWeight: "bold",
                }}
              >
                Log In
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={styles.toSignup}
            onPress={() => navigation.navigate("Signup")}
          >
            <Text style={{ color: "darkblue", fontSize: 22 }}>
              Don't have an account?
            </Text>
            <Text style={{ color: "darkblue", fontSize: 22 }}>
              Click here to create one now!
            </Text>
          </TouchableOpacity>
        </>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  form: {
    marginTop: 25,
    paddingHorizontal: 20,
    justifyContent: "center",
    alignItems: "center",
  },

  inputView: {
    height: 100,
    width: "95%",
    flexDirection: "row",
    justifyContent: "space-evenly",
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 10,
    marginBottom: 25,
    alignItems: "center",
  },

  input: {
    width: "80%",
    borderBottomWidth: 1,
    borderBottomColor: "skyblue",
    fontSize: 18,
  },

  iconView: {
    width: "10%",
    alignSelf: "center",
  },

  loginBtn: {
    borderWidth: 1,
    borderColor: "black",
    marginTop: 10,
    borderRadius: 10,
    paddingHorizontal: 15,
    paddingVertical: 8,
  },

  toSignup: {
    marginTop: 50,
    marginLeft: 30,
  },
});

export default LoginForm;
