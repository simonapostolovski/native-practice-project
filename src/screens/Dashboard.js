import React, { useEffect, useState } from "react";
import { Button, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { handleLogout } from "../redux/Actions/Actions";
import AsyncStorage from "@react-native-async-storage/async-storage";
import firebase from "firebase";
import { LOGGED_IN } from "../redux/Constants/Contstants";
const DashBoard = ({ navigation }) => {
  const { _user, isLoading } = useSelector((state) => ({
    _user: state._user,
    isLoading: state.isLoading,
  }));
  const dispatch = useDispatch();

  // useEffect(() => {
  //   setShownUser(_user);
  //   // init();
  //   // console.log("useristetot", _user);
  // }, []);

  return (
    <View>
      <Text>Dashboard</Text>
      <Text>Hello {_user.displayName}</Text>
      <Text>Hello {_user.email}</Text>
      {/* <Text>Hello {_user.apiKey}</Text> */}
      <Button title="Logout" onPress={() => dispatch(handleLogout())}></Button>
    </View>
  );
};

export default DashBoard;
