import firebase from "firebase";
import React, { useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import LoginForm from "../screens/LoginForm";
import { useDispatch, useSelector } from "react-redux";
import SignUpForm from "../screens/SignupForm";
import DashBoard from "../screens/Dashboard";
import { handleLogout } from "../redux/Actions/Actions";
import { LOADING, LOGGED_IN, SAVE_USER } from "../redux/Constants/Contstants";
import AsyncStorage from "@react-native-async-storage/async-storage";

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const Main = () => {
  const { isLoggedIn, user } = useSelector((state) => ({
    isLoggedIn: state.isLoggedIn,
    _user: state.user,
  }));

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({ type: LOADING });
    const config = {
      apiKey: "AIzaSyDvS3hoU37SX8rwh8lXxMBBGZuAokF_bMs",
      authDomain: "authpracticesimon.firebaseapp.com",
      projectId: "authpracticesimon",
      storageBucket: "authpracticesimon.appspot.com",
      messagingSenderId: "890107132501",
      appId: "1:890107132501:web:d48980d218a625660332dc",
      measurementId: "G-FJ1D60KHK1",
    };
    if (!firebase.apps.length) {
      firebase.initializeApp(config);
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          console.log("user logged in");
          dispatch({
            type: SAVE_USER,
            payload: firebase.auth().currentUser,
          });
          // dispatch({ type: LOGGED_IN });
        } else {
          console.log("no user");
          dispatch(handleLogout());
        }
      });
    } else {
      firebase.app(); // if already initialized, use that one
    }
  }, []);

  return (
    <NavigationContainer>
      {!isLoggedIn ? (
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen name="Login" component={LoginForm} />
          <Stack.Screen name="Signup" component={SignUpForm} />
        </Stack.Navigator>
      ) : (
        <Drawer.Navigator>
          <Drawer.Screen name="Dashboard" component={DashBoard} />
        </Drawer.Navigator>
      )}
    </NavigationContainer>
  );
};

export default Main;
